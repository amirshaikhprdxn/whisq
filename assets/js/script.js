//banner section
var index = 0;
var slide_length = $('.slide').length;
$circle = $(".circle");
$slide = $(".slide");
var circle = $(".circle");
// call change slide function when clicked on circle
Array.from(circle).forEach(function (e,itemIndex){
	e.addEventListener('click',function(event){
		event.preventDefault();
		index = itemIndex;
		changeSlide(index);
	});
});
// show slide image function
function changeSlide(i) {
	$circle.removeClass("active-circle");
	$slide.removeClass("active-slide");
	$circle.eq(i).addClass('active-circle');
	$slide.eq(i).addClass('active-slide');
}
// quote section
$dots = $(".dots");
$blog = $(".blog");
var dots = $(".dots");
// call change slide function when clicked on circle
Array.from(dots).forEach(function (e,itemIndex){
	e.addEventListener('click',function(event){
		event.preventDefault();
		index = itemIndex;
		changeSlide2(index);
	});
});
// show slide image function
function changeSlide2(i) {
	$dots.removeClass("active-dots");
	$blog.removeClass("active-blog");
	$dots.eq(i).addClass('active-dots');
	$blog.eq(i).addClass('active-blog');
}